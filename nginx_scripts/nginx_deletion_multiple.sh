#!/bin/bash
id_ip_mapping_file="/opt/auto_scale_scripts/id_ip_mapping.txt"
instance_to_delete=`cat /opt/auto_scale_scripts/instance_id_to_delete.txt`

ip_to_remove=`grep "$instance_to_delete" "$id_ip_mapping_file" | awk '{print $2}'`
#ip_to_remove=`cat /opt/auto_scale_scripts/ip_to_remove.txt`
while IFS='' read -r line || [[ -n "$line" ]]; do
	cluster_id_mapping_arr=( $line )
	id=${cluster_id_mapping_arr[0]}
	cluster=${cluster_id_mapping_arr[1]}
	curl -X DELETE  --insecure https://api.tenoapp.com/api/3/http/upstreams/"$cluster"/servers/"$id"
done < /opt/auto_scale_scripts/nginx_mappings/"$ip_to_remove"
rm /opt/auto_scale_scripts/nginx_mappings/"$ip_to_remove"


