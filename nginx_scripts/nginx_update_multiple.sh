#!/bin/bash
set -x
ip=`cat /opt/auto_scale_scripts/ip.txt`
echo "$ip"
>../config/nginx_mappings/"$ip"
while IFS='' read -r line  ; do
        cluster_id_mapping_arr=( $line )
        cluster=${cluster_id_mapping_arr[0]}
        port=${cluster_id_mapping_arr[1]}
	CURL_OUT=`curl -X POST -d '{"server":"'"$ip:$port"'"}' --insecure https://api.tenoapp.com/api/3/http/upstreams/"$cluster"/servers`
	CLUSTER_ID=`echo $CURL_OUT | jq '.id'`
	if [[ "$CLUSTER_ID" -ne "null" ]]; then
		echo $CURL_OUT | echo "`jq '.id'` $cluster" >> ../config/nginx_mappings/"$ip"
	else
		echo "ERROR WHILE ADDING $ip $cluster" >> /tmp/nginx_update_curl.log
	fi
	echo $CURL_OUT >> /tmp/nginx_update_curl.log
	done < ../config/cluster_ports_mapping.txt


